// ===================================CV-index.html===================

var a = document.getElementById("lingkaran");
var b = document.getElementById("menu");
var c = document.getElementById("menu");

a.addEventListener("mouseenter", lingkaranmasuk);
b.addEventListener("mouseleave", lingkarankeluar);
c.addEventListener("mouseenter", lingkaranmutar);

function lingkaranmasuk() {
    document.getElementById("menu").style.display = "inline-block";
    document.getElementById("lingkaran").style.transform = "rotate(90deg)";
}

function lingkarankeluar() {
    document.getElementById("menu").style.display = "none";
    document.getElementById("lingkaran").style.transform = "rotate(0deg)";
}

function lingkaranmutar() {
    document.getElementById("lingkaran").style.transform = "rotate(90deg)";
}

function menuutama() {
    document.getElementById("isi").style.transition = "1s";
    document.getElementById("isi").style.left = "0";
    document.getElementById("lingkaran").style.backgroundColor = "black";
    document.getElementById("lingkaran").style.color = "white";
}

function tutupmenuutama() {
    document.getElementById("isi").style.transition = "1s";
    document.getElementById("isi").style.left = "-52.5%";
    document.getElementById("lingkaran").style.backgroundColor = "white";
    document.getElementById("lingkaran").style.color = "black";
}

$('.menubar').click(function() {
  var clicks = $(this).data('clicks');
  if (clicks) {
     menuutama();
  } else {
     tutupmenuutama();
  }
  $(this).data("clicks", !clicks);
});

//==========================MAIN HOME ABOUT CONTACT===============================

var d = document.getElementById("hom");
var e = document.getElementById("hom");

    d.addEventListener("mouseenter", homemasuk);
    e.addEventListener("mouseleave", homekeluar);

function homemasuk() {
    document.getElementById("hom").style.transition = "1s";
    document.getElementById("hom").style.color = "rgb(255,17,17)";
    document.getElementById("hom").style.transition = "1s";
    document.getElementById("wrapisi").style.backgroundColor = "rgba(255,17,17,0.7)";
}

function homekeluar() {
document.getElementById("hom").style.color = "black";
document.getElementById("wrapisi").style.backgroundColor = "transparent";
}

var f = document.getElementById("tentang");
var g = document.getElementById("tentang");

    f.addEventListener("mouseenter", tentangmasuk);
    g.addEventListener("mouseleave", tentangkeluar);

function tentangmasuk() {
    document.getElementById("tentang").style.transition = "1s";
    document.getElementById("tentang").style.color = "rgb(255,246,61)";
    document.getElementById("tentang").style.transition = "1s";
    document.getElementById("wrapisi").style.backgroundColor = "rgba(255,246,61, 0.7)";
}

function tentangkeluar() {
    document.getElementById("tentang").style.color = "black";
    document.getElementById("wrapisi").style.backgroundColor = "transparent";
}

var h = document.getElementById("kontak");
var i = document.getElementById("kontak");

    h.addEventListener("mouseenter", kontakmasuk);
    i.addEventListener("mouseleave", kontakkeluar);

function kontakmasuk() {
    document.getElementById("kontak").style.transition = "1s";
    document.getElementById("kontak").style.color = "rgb(89,234,84)";
    document.getElementById("kontak").style.transition = "1s";
    document.getElementById("wrapisi").style.backgroundColor = "rgba(89,234,84, 0.7)";
}

function kontakkeluar() {
    document.getElementById("kontak").style.color = "black";
    document.getElementById("wrapisi").style.backgroundColor = "transparent";
}

function fotomasuk() {
    document.getElementById("foto").style.top = "450px";
    document.getElementById("tempat").style.top = "260px";
    document.getElementById("tanggal").style.top = "270px";
    document.getElementById("tahun").style.top = "280px";
    document.getElementById("alamatt").style.top = "310px";
    document.getElementById("jalan").style.top = "320px";
    document.getElementById("foto").style.transition = "3s";
    document.getElementById("tempat").style.transition = "5s";
     document.getElementById("tanggal").style.transition = "4s";
    document.getElementById("tahun").style.transition = "3s";
    document.getElementById("alamatt").style.transition = "2s";
    document.getElementById("jalan").style.transition = "1s";
}

function fotokeluar() {
    document.getElementById("foto").style.top = "-300px";
    document.getElementById("tempat").style.top = "-700px";
    document.getElementById("tanggal").style.top = "-700px";
    document.getElementById("tahun").style.top = "-700px";
    document.getElementById("alamatt").style.top = "-700px";
    document.getElementById("jalan").style.top = "-700px";
    document.getElementById("foto").style.transition = "3s";
    document.getElementById("tempat").style.transition = "5s";
    document.getElementById("tanggal").style.transition = "4s";
    document.getElementById("tahun").style.transition = "3s";
    document.getElementById("alamatt").style.transition = "2s";
    document.getElementById("jalan").style.transition = "1s";
}

$('#hom').click(function() {
  var clicks = $(this).data('clicks');
  if (clicks) {
     fotomasuk();
  } else {
     fotokeluar();
  }
  $(this).data("clicks", !clicks);
});

function kontakklik() {
    document.getElementById("kontakmailis").style.top = "330px";
    document.getElementById("kontaktelp").style.top = "340px";
    document.getElementById("kontakmailis").style.transition = "2s";
    document.getElementById("kontaktelp").style.transition = "1s";
}

function kontakkklok() {
    document.getElementById("kontakmailis").style.top = "-700px";
    document.getElementById("kontaktelp").style.top = "-700px";
    document.getElementById("kontakmailis").style.transition = "2s";
    document.getElementById("kontaktelp").style.transition = "1s";
}

$('#kontak').click(function() {
  var clicks = $(this).data('clicks');
  if (clicks) {
     kontakklik();
  } else {
     kontakkklok();
  }
  $(this).data("clicks", !clicks);
});

function aboutklik() {
    document.getElementById("about").style.top = "350px";
    document.getElementById("about").style.transition = "2s";
}

function aboutklok() {
    document.getElementById("about").style.top = "-700px";
    document.getElementById("about").style.transition = "2s";
}



$('#tentang').click(function() {
  var clicks = $(this).data('clicks');
  if (clicks) {
     aboutklik();
  } else {
     aboutklok();
  }
  $(this).data("clicks", !clicks);
});





